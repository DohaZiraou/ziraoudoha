package com.example.exam.Model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Client extends Personne{
    @OneToMany(mappedBy="client",fetch = FetchType.EAGER)
    private List<Reservation> reservations =new ArrayList<>();

    public Client() {
    }

    public Client(String nom) {
        super(nom);
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public String toString() {
        return "Client{" +
                "reservations=" + reservations +
                '}';
    }
}
