package com.example.exam.Model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Compagnie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    @OneToMany(mappedBy="compagnie",fetch = FetchType.EAGER)
    private List<Vol> vols = new ArrayList<>();

    public Compagnie() {
    }

    public Compagnie(String nom) {
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Vol> getVols() {
        return vols;
    }

    public void setVols(List<Vol> vols) {
        this.vols = vols;
    }

    @Override
    public String toString() {
        return "Compagnie{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", vols=" + vols +
                '}';
    }
}
