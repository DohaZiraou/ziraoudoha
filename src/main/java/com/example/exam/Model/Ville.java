package com.example.exam.Model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Ville {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String nom;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="ville_aeroport",
            joinColumns = @JoinColumn(name = "ville_id"),
            inverseJoinColumns = @JoinColumn(name = "aeroport_id")
    )
    private List<Aeroport> aeroports = new ArrayList<>();

    public Ville() {
    }

    public Ville(String nom) {
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Aeroport> getAeroports() {
        return aeroports;
    }

    public void setAeroports(List<Aeroport> aeroports) {
        this.aeroports = aeroports;
    }

    @Override
    public String toString() {
        return "Ville{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", aeroports=" + aeroports +
                '}';
    }
}
