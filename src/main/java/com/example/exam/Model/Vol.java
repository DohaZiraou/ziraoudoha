package com.example.exam.Model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Vol {
    @Id
    private Long numVol;
    private String jourDepart;
    private String heureDepart;
    private String jourArrive;
    private String heureArrive;
    @OneToMany(mappedBy = "vol",fetch = FetchType.EAGER)
    private List<Reservation> reservations = new ArrayList<>();
    @ManyToOne
    @JoinColumn(name = "compagnie_id")
    private Compagnie compagnie;
    @ManyToOne
    @JoinColumn(name="aeroport_depart_id")
    private Aeroport aeroportDepart;
    @ManyToOne
    @JoinColumn(name="aeroport_arrive_id")
    private Aeroport aeroportArrive;

    @OneToMany(mappedBy="vol",fetch = FetchType.EAGER)
    private List<Escale> escales= new ArrayList<>();

    public Vol() {
    }

    public Vol(Long numVol, String jourDepart, String heureDepart, String jourArrive, String heureArrive) {
        this.numVol = numVol;
        this.jourDepart = jourDepart;
        this.heureDepart = heureDepart;
        this.jourArrive = jourArrive;
        this.heureArrive = heureArrive;
    }

    public Long getNumVol() {
        return numVol;
    }

    public void setNumVol(Long numVol) {
        this.numVol = numVol;
    }

    public String getJourDepart() {
        return jourDepart;
    }

    public void setJourDepart(String jourDepart) {
        this.jourDepart = jourDepart;
    }

    public String getHeureDepart() {
        return heureDepart;
    }

    public void setHeureDepart(String heureDepart) {
        this.heureDepart = heureDepart;
    }

    public String getJourArrive() {
        return jourArrive;
    }

    public void setJourArrive(String jourArrive) {
        this.jourArrive = jourArrive;
    }

    public String getHeureArrive() {
        return heureArrive;
    }

    public void setHeureArrive(String heureArrive) {
        this.heureArrive = heureArrive;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public Compagnie getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }

    public Aeroport getAeroportDepart() {
        return aeroportDepart;
    }

    public void setAeroportDepart(Aeroport aeroportDepart) {
        this.aeroportDepart = aeroportDepart;
    }

    public Aeroport getAeroportArrive() {
        return aeroportArrive;
    }

    public void setAeroportArrive(Aeroport aeroportArrive) {
        this.aeroportArrive = aeroportArrive;
    }

    public List<Escale> getEscales() {
        return escales;
    }

    public void setEscales(List<Escale> escales) {
        this.escales = escales;
    }

    @Override
    public String toString() {
        return "Vol{" +
                "numVol=" + numVol +
                ", jourDepart='" + jourDepart + '\'' +
                ", heureDepart='" + heureDepart + '\'' +
                ", jourArrive='" + jourArrive + '\'' +
                ", heureArrive='" + heureArrive + '\'' +
                ", reservations=" + reservations +
                ", compagnie=" + compagnie +
                ", aeroportDepart=" + aeroportDepart +
                ", aeroportArrive=" + aeroportArrive +
                ", escales=" + escales +
                '}';
    }
}
