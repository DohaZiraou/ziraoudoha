package com.example.exam.Model;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Passager extends Personne{
    @OneToMany(mappedBy = "passager",fetch = FetchType.EAGER)
    private List<Reservation> reservations=new ArrayList<>();

    public Passager() {
    }

    public Passager(String nom) {
        super(nom);
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public String toString() {
        return "Passager{" +
                "reservations=" + reservations +
                '}';
    }
}
