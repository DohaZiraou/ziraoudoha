package com.example.exam.Model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class EscalePK implements Serializable {
    @Column(name = "vol_id")
    private Long vol;
    @Column(name= "aeroport_id")
    private Long aeroport;
}
