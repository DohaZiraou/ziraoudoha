package com.example.exam.Model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Aeroport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    @OneToMany(mappedBy = "aeroportDepart",fetch = FetchType.EAGER)
    private List<Vol> volsDepart = new ArrayList<>();
    @OneToMany(mappedBy = "aeroportArrive",fetch = FetchType.EAGER)
    private List<Vol> volsArrive = new ArrayList<>();
    @OneToMany(mappedBy = "aeroport",fetch = FetchType.EAGER)
    private List<Escale> escales = new ArrayList<>();

    @ManyToMany(mappedBy="aeroports",fetch = FetchType.EAGER)
    private List<Ville> villes = new ArrayList<>();

    public Aeroport() {
    }

    public Aeroport(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Vol> getVolsDepart() {
        return volsDepart;
    }

    public void setVolsDepart(List<Vol> volsDepart) {
        this.volsDepart = volsDepart;
    }

    public List<Vol> getVolsArrive() {
        return volsArrive;
    }

    public void setVolsArrive(List<Vol> volsArrive) {
        this.volsArrive = volsArrive;
    }

    public List<Escale> getEscales() {
        return escales;
    }

    public void setEscales(List<Escale> escales) {
        this.escales = escales;
    }

    public List<Ville> getVilles() {
        return villes;
    }

    public void setVilles(List<Ville> villes) {
        this.villes = villes;
    }

    @Override
    public String toString() {
        return "Aeroport{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", volsDepart=" + volsDepart +
                ", volsArrive=" + volsArrive +
                ", escales=" + escales +
                ", villes=" + villes +
                '}';
    }
}
