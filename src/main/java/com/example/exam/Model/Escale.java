package com.example.exam.Model;

import jakarta.persistence.*;

@Entity
public class Escale {
    @EmbeddedId
    private EscalePK id;
    private String heureArrive;
    private String heureDepart;
    @ManyToOne
    @JoinColumn(name="vole_id")
    private Vol vol;
    @ManyToOne
    @JoinColumn(name="aeroporte_id")

    private Aeroport aeroport;

    public Escale() {
    }

    public Escale(String heureArrive, String heureDepart) {
        this.heureArrive = heureArrive;
        this.heureDepart = heureDepart;
    }

    public String getHeureArrive() {
        return heureArrive;
    }

    public void setHeureArrive(String heureArrive) {
        this.heureArrive = heureArrive;
    }

    public String getHeureDepart() {
        return heureDepart;
    }

    public void setHeureDepart(String heureDepart) {
        this.heureDepart = heureDepart;
    }

    public Vol getVol() {
        return vol;
    }

    public void setVol(Vol vol) {
        this.vol = vol;
    }

    public Aeroport getAeroport() {
        return aeroport;
    }

    public void setAeroport(Aeroport aeroport) {
        this.aeroport = aeroport;
    }

    @Override
    public String toString() {
        return "Escale{" +
                "id=" + id +
                ", heureArrive='" + heureArrive + '\'' +
                ", heureDepart='" + heureDepart + '\'' +
                ", vol=" + vol +
                ", aeroport=" + aeroport +
                '}';
    }
}
