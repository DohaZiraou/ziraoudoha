package com.example.exam.Controlleur;

import com.example.exam.Model.Passager;
import com.example.exam.Repository.PassagerRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("exam")
public class PassagerControlleur {
    PassagerRepository passagerRepository;
    PassagerControlleur(PassagerRepository passagerRepository){
        this.passagerRepository=passagerRepository;
    }
    @GetMapping("/passagers")
    public List<Passager> passagers(){
        return passagerRepository.findAll();
    }
    @PostMapping("/passager")
    public Passager ajouterPassager(@RequestBody Passager passager){
        return passagerRepository.save(passager);
    }
    @PutMapping("/{id}")
    public void modifierPassager(@RequestBody Passager p,@PathVariable Long id) {
        passagerRepository.findById(id)
                .map(passager -> {
                    passager.setNom(p.getNom());
                    return passagerRepository.save(passager);
                })
        .orElseGet(()->passagerRepository.save(p));
    }
    @DeleteMapping("/{id}")
    public void supprimerPassager(@PathVariable Long id){
        passagerRepository.deleteById(id);
    }
}
