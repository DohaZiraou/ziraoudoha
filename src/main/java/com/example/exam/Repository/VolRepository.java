package com.example.exam.Repository;

import com.example.exam.Model.Vol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VolRepository extends JpaRepository<Vol,Long> {
}
