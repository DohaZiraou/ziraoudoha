package com.example.exam.Repository;

import com.example.exam.Model.Passager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassagerRepository extends JpaRepository<Passager,Long> {
}
