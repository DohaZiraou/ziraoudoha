package com.example.exam.Repository;

import com.example.exam.Model.Escale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EscaleRepository extends JpaRepository<Escale,Long> {
}
