package com.example.exam.Repository;

import com.example.exam.Model.Aeroport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AeroportRepository extends JpaRepository<Aeroport,Long> {
}
