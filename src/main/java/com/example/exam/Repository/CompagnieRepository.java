package com.example.exam.Repository;

import com.example.exam.Model.Compagnie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompagnieRepository extends JpaRepository<Compagnie,Long> {
}
